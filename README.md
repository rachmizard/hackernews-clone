# Hacker News Clone

As a challenge required for job application as a Frontend Engineer at Sampingan this source implements the APIs available at https://github.com/HackerNews/API such as reading top stories data, latest stories and best stories, as well as reading its comments in it and providing job data as well.

## Tech & Plugins

This project uses a number of open source projects to work properly:

- [React JS] - is open source Javascript library for building user interfaces SPA bootstrapped with Create React App
- [React Router Dom] - awesome router for SPA react render page fast and scalable

| Plugin                 | README                                                                     |
| ---------------------- | -------------------------------------------------------------------------- |
| Styled Component       | https://github.com/styled-components/styled-components/blob/main/README.md |
| React Loading Skeleton | https://www.npmjs.com/package/react-loading-skeleton                       |
| Axios                  | https://github.com/axios/axios/blob/master/README.md                       |

## Installation with docker

Follow this require steps :

```sh
git clone git@gitlab.com:rachmizard/hackernews-clone.git
cd hackernews-clone
docker build -f Dockerfile -t hn-clone .
docker run -p 8000:80 hn-clone
```

## Installation without docker

Hackernews Clone requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the server.

```sh
git clone git@gitlab.com:rachmizard/hackernews-clone.git
cd hackernews-clone
npm install
npm start
```

## Description Folder & File Structure

### src/

###### App.js

App.js is a main entry point, routing global style and theme are defined here.

###### index.js

index.js is main root for rendeing React DOM.

### components/

components folder is divided into two category of component, UI and Component stateful/reuse. Component UI contains interface such as layouting/navbar/containers besides that Component Stateful will reuse in another Page or containers.

###### UI/Container.jsx

Container works to wrap the block in it main content, so it looks fit against the screen

###### UI/Layout.jsx

Layout wraps all container elements in it and organize UI components such as Navbar MainContent and Container and will be used in whole of project page.

###### UI/Navbar.jsx

Navbar is a header menu on the top of screen and display menu link.

###### Comment.jsx

This components will be used in page also will reuse in many times for display comment data also has implemented stateful component.

###### Story.jsx

This components will be used in page also will reuse in many times for display news/story data also has implemented stateful component.

###### User.jsx

This components will be used in page also will reuse in another page for display user information also has implemented stateful component.

### constant/

This directory contains a working index.js file that contains several configuration.

### hooks/

Hooks folder will be used across the project for isolated code implementation.

###### useDataFetcher.js

usaDataFetcher is a customize hook for integration local state array within server side such as retrieve storyIds only.

###### useFetchItem.js

useFetchItem is a customize hook for integration local state object within server to get item story.

###### useInfiniteScroll.js

Infinite scrolling is a web-design technique that loads content continuously as the user scrolls down the page.

###### useUserFecther.js

An hook for get a user item from server and passed to local state.

###### useIntersectionObserve.js

An Intersection Observer is a browser API that provides a way to observe the visibility and position of a DOM element relative to the containing root element or viewport.

### pages/

This folder contains component to rendering page that already integrated with react router. Page contains (404 page, ErrorFallback, HomePage, StoryDetail, etc...)

### routes/

This folder contains a working index.js file to define path routes here.

### services/

This folder contains axios configuration, and hackernews API flow business API.

### styles/

This folder contains style across components also globalstyling using plugin Styled Components.

### utils/

This folder will be used across project. such as utility helpers function.

###### mapTimer.js

Map timer provides as convertDate and differenceDate converter from unix timestamp.

###### debouncer.js

Debouncer is a practice used to ensure that time-consuming tasks do not fire so often.
