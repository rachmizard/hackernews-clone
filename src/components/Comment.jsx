import React, { useState, memo } from "react";
import PropTypes from "prop-types";
import {
  CommentCard,
  CommentInfo,
  CommentContent,
  CollapseButton,
  CommentKidsWrapper,
} from "styles/Comment.style";
import Skeleton from "react-loading-skeleton";
import { mapTimer } from "utils";
import { useFetcherItem } from "hooks";
import { Link } from "react-router-dom";

const Comment = memo(function Comment({ id }) {
  const [data, loading] = useFetcherItem(id);
  const [displayChild, setDisplayChild] = useState(false);

  const onToggleDisplayChild = () => {
    setDisplayChild(!displayChild);
  };

  const dataBy = data?.by;
  const dataTime = mapTimer.diffDate(data?.time);
  const dataText = data?.text;
  const dataType = data?.type;
  const haveKids = data?.kids && data?.kids.length > 0;

  if (!data) return null;

  if (dataType !== "comment") return null;

  return (
    <CommentCard>
      {loading ? (
        <>
          <Skeleton width="100%" />
          <Skeleton width="100%" />
        </>
      ) : (
        <>
          <CommentInfo>
            <Link to={`/user/${dataBy}`}>{dataBy}</Link>
            <span> - {dataTime}</span>
          </CommentInfo>
          <CommentContent dangerouslySetInnerHTML={{ __html: dataText }} />
        </>
      )}
      {haveKids && (
        <CollapseButton onClick={onToggleDisplayChild} collapsed={displayChild}>
          &#8250;
        </CollapseButton>
      )}
      <CommentKidsWrapper collapsed={displayChild}>
        {haveKids && data.kids.map((kid) => <Comment key={kid} id={kid} />)}
      </CommentKidsWrapper>
    </CommentCard>
  );
});

Comment.propTypes = {
  id: PropTypes.number.isRequired,
};

export default Comment;
