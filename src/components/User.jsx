import React from "react";
import { Link } from "react-router-dom";
import { UserWrapper, UserInfo, UserOtherInfo } from "styles/User.style";
import Skeleton from "react-loading-skeleton";
import { convertedTime } from "utils/mapTimer";

export default function User({ data, loading }) {
  const submissionRoutePath = `/user/${data?.id}/submissions`;
  const commentRoutePath = `/user/${data?.id}/comments`;

  return (
    <UserWrapper>
      {loading ? (
        <>
          <Skeleton width="100%" />
          <Skeleton width="100%" />
          <Skeleton width="100%" />
          <Skeleton width="100%" />
        </>
      ) : (
        <>
          <h1>{data.id}</h1>
          <UserInfo>
            <h5>User ID</h5>
            <span>{data.id}</span>
          </UserInfo>
          <UserInfo>
            <h5>Created At</h5>
            <span>{convertedTime(data.created)}</span>
          </UserInfo>
          <UserInfo>
            <h5>Karma</h5>
            <span>{data.karma}</span>
          </UserInfo>
          <UserInfo>
            <h5>About</h5>
            <p dangerouslySetInnerHTML={{ __html: data.about }} />
          </UserInfo>
          <UserInfo>
            <h5>Other Info</h5>
            <UserOtherInfo>
              <Link to={submissionRoutePath}>Submissions</Link>
              <Link to={commentRoutePath}>Comments</Link>
            </UserOtherInfo>
          </UserInfo>
        </>
      )}
    </UserWrapper>
  );
}
