import React from "react";
import PropTypes from "prop-types";
import { useIntersectionObserve } from "hooks";
import { NavbarWrapper, NavbarLinksWrapper } from "styles/Navbar.style";
import { NavLink } from "react-router-dom";

export default function Navbar({ children }) {
  const [containerRef, isVisible] = useIntersectionObserve({
    root: null,
    threshold: 1.0,
  });

  return (
    <NavbarWrapper ref={containerRef} scrolled={isVisible}>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          scrolled: isVisible,
        });
      })}
    </NavbarWrapper>
  );
}

export function NavbarLinks({ scrolled }) {
  return (
    <NavbarLinksWrapper scrolled={scrolled}>
      <NavLink className="nav__link" to="/top">
        Top Stories
      </NavLink>
      <NavLink className="nav__link" to="/new">
        Latest Stories
      </NavLink>
      <NavLink className="nav__link" to="/best">
        Best Stories
      </NavLink>
      <NavLink className="nav__link" to="/ask">
        Ask
      </NavLink>
      <NavLink className="nav__link" to="/job">
        Jobs
      </NavLink>
    </NavbarLinksWrapper>
  );
}

Navbar.propTypes = {
  children: PropTypes.node.isRequired,
};
