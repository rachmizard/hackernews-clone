import React from "react";
import PropTypes from "prop-types";
import { ContainerWrapper } from "styles/Container.style";

export default function Container({ children }) {
  return <ContainerWrapper>{children}</ContainerWrapper>;
}

Container.propTypes = {
  children: PropTypes.node,
};
