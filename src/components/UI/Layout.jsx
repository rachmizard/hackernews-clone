import React from "react";
import PropTypes from "prop-types";
import {
  LayoutWrapper,
  Title,
  TitleLogo,
  MainContent,
} from "styles/Layout.style";
import { Container } from "./";
import Navbar, { NavbarLinks } from "./Navbar";

export default function Layout({ children }) {
  return (
    <LayoutWrapper>
      <Title>
        <TitleLogo>Y</TitleLogo> Hacker News Clone
      </Title>
      <Navbar>
        <NavbarLinks />
      </Navbar>
      <Container>
        <MainContent>{children}</MainContent>
      </Container>
    </LayoutWrapper>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
