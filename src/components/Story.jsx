import { memo } from "react";
import { useFetcherItem } from "hooks";
import PropTypes from "prop-types";
import Skeleton from "react-loading-skeleton";
import { Link } from "react-router-dom";
import {
  StoryWrapper,
  StoryTitle,
  StoryMeta,
  StoryInfo,
} from "styles/Story.style";
import { mapTimer } from "utils";

const Story = memo(function Story({ id, mode }) {
  const [data, loading] = useFetcherItem(id);

  const dataUrl = data?.url;
  const dataScore = data?.score;
  const dataBy = data?.by;
  const dataTime = mapTimer.diffDate(data?.time);
  const dataType = data?.type;

  if (!data) return null;

  if (data.deleted || data.dead) return null;

  if (mode === "userpage" && dataType !== "story") return null;

  if (dataType === "job") {
    return (
      <StoryWrapper>
        {loading ? (
          <>
            <Skeleton count={1} width="100%" />
            <Skeleton count={1} width="50%" />
          </>
        ) : (
          <>
            <StoryTitle href={dataUrl} target="_blank">
              {data.title}
            </StoryTitle>
            <StoryMeta>
              <Link className="story-info-link" to={`/user/${data.by}`}>
                By : <span>{dataBy}</span>
              </Link>
              <StoryInfo show>{dataTime}</StoryInfo>
            </StoryMeta>
          </>
        )}
      </StoryWrapper>
    );
  } else {
    return (
      <StoryWrapper>
        {loading ? (
          <>
            <Skeleton count={1} width="100%" />
            <Skeleton count={1} width="50%" />
          </>
        ) : (
          <>
            <StoryTitle href={dataUrl} target="_blank">
              {data.title}
            </StoryTitle>
            <StoryMeta>
              <StoryInfo>
                <span>{dataScore}</span> points
              </StoryInfo>
              <Link className="story-info-link" to={`/user/${data.by}`}>
                By : <span>{dataBy}</span>
              </Link>
              <StoryInfo show>{dataTime}</StoryInfo>
              <Link className="story-info-link right" to={`/item/${data.id}`}>
                {data.descendants} comments
              </Link>
            </StoryMeta>
          </>
        )}
      </StoryWrapper>
    );
  }
});

Story.propTypes = {
  id: PropTypes.any.isRequired,
  mode: PropTypes.string,
};

export default Story;
