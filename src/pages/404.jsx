import React from "react";
import { useLocation } from "react-router-dom";
import { Page404Wrapper } from "styles/Page404.style";

export default function Page404() {
  const { pathname } = useLocation();

  return (
    <Page404Wrapper>
      <p>
        Page not found 404 with url : <b>{pathname}</b>
      </p>
    </Page404Wrapper>
  );
}
