import React from "react";
import { Story, Comment } from "components";
import { useFetcherItem, useInfiniteScroll } from "hooks";

export default function StoryDetailPage({ match }) {
  const { itemId } = match.params;
  const [story] = useFetcherItem(itemId);
  const [count] = useInfiniteScroll();

  const haveKids = story.kids && story.kids.length > 0;

  return (
    <>
      <Story id={itemId} />
      {haveKids &&
        story.kids
          .slice(0, count)
          .map((kid, i) => <Comment key={i} id={kid} />)}
    </>
  );
}
