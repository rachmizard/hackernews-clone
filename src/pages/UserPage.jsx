import { User } from "components";
import { useUserFetcher } from "hooks";
import React from "react";

export default function UserPage({ match }) {
  const { userId } = match.params;
  const [data, loading] = useUserFetcher(userId);

  return <User data={data} loading={loading} />;
}
