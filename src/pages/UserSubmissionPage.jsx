import React from "react";
import { Story, User } from "components";
import { useInfiniteScroll, useUserFetcher } from "hooks";

export default function UserSubmissionPage({ match }) {
  const { userId } = match.params;
  const [data, loading] = useUserFetcher(userId);
  const [count] = useInfiniteScroll();

  return (
    <>
      <User data={data} loading={loading} />
      <h4 style={{ textAlign: "center" }}>Submissions</h4>
      {data.submitted
        ? data.submitted
            .slice(0, count)
            .map((id) => <Story key={id} mode="userpage" id={id} />)
        : null}
    </>
  );
}
