import React from "react";
import { ErrorButton, ErrorFallbackWrapper } from "styles/ErrorFallback.style";

export default function ErrorFallback({ error, resetErrorBoundary }) {
  return (
    <ErrorFallbackWrapper role="alert">
      <h1>Oopps! Something went wrong!</h1>
      <pre>{error.message}</pre>
      <ErrorButton onClick={resetErrorBoundary}>Try again</ErrorButton>
    </ErrorFallbackWrapper>
  );
}
