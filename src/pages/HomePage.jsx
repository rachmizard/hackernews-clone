import React from "react";
import { useDataFetcher, useInfiniteScroll } from "hooks";
import { Story } from "components";

export default function HomePage(props) {
  const { type } = props.match.params;
  const { data } = useDataFetcher(type);
  const [count] = useInfiniteScroll();

  return data.slice(0, count).map((idx) => <Story key={idx} id={idx} />);
}
