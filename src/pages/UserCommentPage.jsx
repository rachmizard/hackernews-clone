import React from "react";
import { Comment, User } from "components";
import { useInfiniteScroll, useUserFetcher } from "hooks";

export default function UserCommentPage({ match }) {
  const { userId } = match.params;
  const [data, loading] = useUserFetcher(userId);
  const [count] = useInfiniteScroll();
  const haveSubmitted = data?.submitted && data.submitted.length > 0;

  return (
    <>
      <User data={data} loading={loading} />
      <h4 style={{ textAlign: "center" }}>Comments</h4>

      {haveSubmitted
        ? data.submitted
            .slice(0, count)
            .map((id) => <Comment key={id} type="comment" id={id} />)
        : null}
    </>
  );
}
