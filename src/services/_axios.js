import axios from "axios";

const serverConfiguration = {
  BASE_URL: "https://hacker-news.firebaseio.com/",
  VERSION: "v0",
};

const _axios = axios;
_axios.defaults.baseURL = `${serverConfiguration.BASE_URL}${serverConfiguration.VERSION}`;

export default _axios;
