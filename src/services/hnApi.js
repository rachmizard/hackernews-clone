import { _axios } from "./";

export const getStories = async (type) => {
  try {
    const { data } = await _axios.get(`${type}stories.json`);

    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const getStory = async (itemIdx) => {
  try {
    const { data } = await _axios.get(`item/${itemIdx}.json`);

    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const getUser = async (userId) => {
  try {
    const { data } = await _axios.get(`user/${userId}.json`);

    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};
