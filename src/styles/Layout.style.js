import styled from "styled-components";

export const LayoutWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const Title = styled.h1`
  text-align: center;
  color: ${(props) => props.theme.colors.primary};
  margin: 0.5em 0px 0.5em 0px;
`;

export const TitleLogo = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  padding-inline: 0.3em;
  background-color: ${(props) => props.theme.colors.primary};
  color: white;
`;

export const MainContent = styled.main`
  width: 100%;
  min-height: 100vh;
`;
