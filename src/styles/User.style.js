import styled from "styled-components";

export const UserWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1em;
  margin: 0.8em 0em 0.8em 0em;
  background-color: white;
  border-radius: 10px;
  justify-content: space-between;
  box-shadow: 0px 1px 4px 0 rgba(0, 0, 0, 0.2);
  width: 50%;
  gap: 20px;
`;

export const UserInfo = styled.div`
  display: flex;
  flex-direction: column;
  gap: 7px;

  > h5 {
    color: gray;
    font-weight: 500;
    font-size: 14px;
  }
`;

export const UserOtherInfo = styled.div`
  display: flex;
  gap: 10px;

  > a {
    color: black;
    font-size: 14px;
  }
`;
