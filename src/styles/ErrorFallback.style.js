import styled from "styled-components";

export const ErrorFallbackWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  min-height: 100vh;
`;

export const ErrorButton = styled.button`
  display: inline-block;
  border: 1px solid ${(props) => props.theme.colors.primary};
  padding: 0.8em;
  background-color: ${(props) => props.theme.colors.primary};
  color: white;
  font-weight: bolder;
  border-radius: 4px;
  cursor: pointer;

  :hover {
    color: gray;
  }
`;
