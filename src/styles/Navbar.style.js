import styled from "styled-components";

export const NavbarWrapper = styled.nav`
  display: flex;
  flex-direction: column;
  width: 100%;
  position: sticky;
  transition: all 0.25s ease-in-out;
  background-color: ${(props) =>
    !props.scrolled ? props.theme.colors.primary : "transparent"};
  top: -1px;
  z-index: 999;
  ${(props) => !props.scrolled && "box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);"}
`;

export const NavbarLinksWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 65%;
  margin: auto;
  padding: 1em;

  > .nav__link {
    color: ${(props) =>
      !props.scrolled ? "white" : props.theme.colors.primary};
    text-decoration: none;
    padding: 0.5em;
    font-weight: bold;
  }

  > .nav__link:hover {
    transition: all 0.25s ease-in-out;
    color: gray;
  }

  > .nav__link:hover::after {
    content: "";
  }

  > .nav__link:after {
    content: "";
    width: 100%;
    height: 10px;
  }

  > .active {
    color: ${(props) =>
      !props.scrolled ? "black" : props.theme.colors.darkgray};
  }
`;
