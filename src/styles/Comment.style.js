import styled from "styled-components";

export const CommentCard = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  margin: 0.3em;
`;

export const CommentInfo = styled.p`
  color: ${({ theme }) => theme.colors.darkgray};
  font-weight: bolder;
  font-size: 12px;

  > a {
    color: gray;
    text-decoration: none;
  }

  > a:hover {
    text-decoration: underline;
  }

  > span {
    font-weight: normal;
  }
`;

export const CommentContent = styled.div`
  color: black;
  font-weight: 400;
  line-height: 2em;
  font-size: 12px;
  box-shadow: 0px 1px 4px 0 rgba(0, 0, 0, 0.2);
  background-color: white;
  padding: 1em;
  border-radius: 5px;
`;

export const CollapseButton = styled.button`
  display: inline-block;
  border: none;
  width: 35px;
  padding: 0.5rem;
  font-size: 16px;
  font-weight: bold;
  cursor: pointer;
  border-radius: 100%;
  color: white;
  transition: all 0.2s;
  transform: ${(props) => (props.collapsed ? "rotate(0deg)" : "rotate(90deg)")};
  background-color: ${({ theme }) => theme.colors.primary};
`;

export const CommentKidsWrapper = styled.div`
  margin-left: 22px;
  border-left: 2px solid gray;
  padding-left: 20px;
  height: 100%;
  display: ${(props) => (props.collapsed ? "none" : "block")};
`;
