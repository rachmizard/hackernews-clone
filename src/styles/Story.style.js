import styled from "styled-components";

export const StoryWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 3em;
  padding: 1em;
  margin: 0.8em 0em 0.8em 0em;
  background-color: white;
  border-radius: 5px;
  justify-content: space-between;
  box-shadow: 0px 1px 4px 0 rgba(0, 0, 0, 0.2);
`;

export const StoryTitle = styled.a`
  color: ${(props) => props.theme.colors.primary};
  text-decoration: none;
  transition: all 0.25s;
  font-weight: bold;
  font-size: 16px;

  :hover {
    color: black;
  }
`;

export const StoryMeta = styled.div`
  display: flex;
  align-items: center;

  > .story-info-link {
    margin-right: 1em;
    color: white;
    font-size: 12px;
    padding: 0.2em 0.6em 0.2em 0.6em;
    border-radius: 50px;
    background-color: ${(props) => props.theme.colors.primary};
    transition: 0.2s;
    text-decoration: none;

    span {
      font-weight: bold;
    }

    &:hover {
      text-decoration: underline;
    }
  }

  > .story-info-link:last-of-type {
    margin-left: auto;
    background-color: gray;
  }
`;

export const StoryInfo = styled.a`
  display: ${(props) => (props.show ? "inline-block" : "none")};
  margin-right: 1em;
  color: white;
  font-size: 12px;
  padding: 0.2em 0.6em 0.2em 0.6em;
  border-radius: 50px;
  background-color: ${(props) => props.theme.colors.primary};
  transition: 0.2s;

  span {
    font-weight: bold;
  }

  &:hover {
    background-color: white;
    color: black;
    transform: translateY(-5%);
  }
`;
