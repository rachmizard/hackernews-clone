export const diffDate = (unix) => {
  const cur = new Date();
  const curUnix = Math.floor(cur.getTime() / 1000); // in seconds
  const d = curUnix - unix; // in seconds

  // in seconds
  const minute = 60;
  const hour = minute * 60;
  const day = hour * 24;
  const threeMonths = day * 63;
  const year = day * 365;

  // less than an hour
  if (d < hour) {
    const minuteAgo = Math.floor(d / minute);
    return `${minuteAgo} minute${minuteAgo > 1 ? "s" : ""} ago`;
  }

  // more than an hour, less than a day
  else if (d < day) {
    const hourAgo = Math.floor(d / hour);
    return `${hourAgo} hour${hourAgo > 1 ? "s" : ""} ago`;
  }
  // more than a day, less than 63 days
  else if (d < threeMonths) {
    const dayAgo = Math.floor(d / day);
    return `${dayAgo} day${dayAgo > 1 ? "s" : ""} ago`;
  }
  // more than 63 days, less than a year
  else if (d < year) {
    const monthAgo = Math.floor(d / (day * 30));
    return `${monthAgo} month${monthAgo > 1 ? "s" : ""} ago`;
  }
  // more than a year
  else {
    const yearAgo = Math.floor(d / (day * 365));
    return `${yearAgo} year${yearAgo > 1 ? "s" : ""} ago`;
  }
};

export function convertedTime(unixTimeStamp) {
  const a = new Date(unixTimeStamp * 1000);
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const year = a.getFullYear();
  const month = months[a.getMonth()];
  const date = a.getDate();
  const hour = a.getHours();
  const min = a.getMinutes();
  const sec = a.getSeconds();
  const time =
    date + " " + month + " " + year + " " + hour + ":" + min + ":" + sec;

  return time;
}
