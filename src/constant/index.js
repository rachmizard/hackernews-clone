export const categories = ["top", "new", "best", "comment", "ask", "job"];
export const INCREMENT_DATA = 10;

export const theme = {
  colors: {
    primary: "#fb683a",
    secondary: "#CAC6C6",
    lightgray: "#E5E3E3",
    darkgray: "#958D8D",
  },
};
