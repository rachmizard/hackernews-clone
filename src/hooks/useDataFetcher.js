import { useCallback, useEffect, useState } from "react";
import { getStories } from "services/hnApi";

function useDataFetcher(type) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchData = useCallback(() => {
    setLoading(true);
    getStories(type).then((data) => {
      setData(data);
      setLoading(true);
    });
  }, [type]);

  useEffect(() => {
    let subscribe = true;

    if (subscribe) {
      fetchData();
    }

    return () => (subscribe = false);
  }, [fetchData, type]);

  return {
    data,
    loading,
  };
}

export default useDataFetcher;
