import { useState, useEffect } from "react";
import { debouncer } from "utils";
import { INCREMENT_DATA } from "constant";

function useInfiniteScroll() {
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(INCREMENT_DATA);

  const handleScroll = debouncer(() => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
        document.documentElement.offsetHeight ||
      loading
    ) {
      return false;
    }

    setLoading(true);
  }, 500);

  useEffect(() => {
    if (!loading) return;

    setCount(count + INCREMENT_DATA);

    setLoading(false);
  }, [count, loading]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [handleScroll]);

  return [count, loading];
}

export default useInfiniteScroll;
