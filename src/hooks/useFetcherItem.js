import { useState, useCallback, useEffect } from "react";
import { getStory } from "services/hnApi";

function useFetcherItem(idx) {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  const fetchData = useCallback(() => {
    setLoading(true);
    getStory(idx).then((data) => {
      setData(data);
      setLoading(false);
    });
  }, [idx]);

  useEffect(() => {
    let subscribe = true;

    if (subscribe) {
      fetchData();
    }

    return () => (subscribe = false);
  }, [fetchData]);

  return [data, loading];
}

export default useFetcherItem;
