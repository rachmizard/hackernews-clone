import { useState, useEffect, useRef } from "react";

function useIntersectionObserve(options) {
  const containerRef = useRef(null);

  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    let observerRefValue = null;

    const callback = (entries) => {
      const [entry] = entries;
      setIsVisible(entry.isIntersecting);
    };

    const observer = new IntersectionObserver(callback, options);

    if (containerRef.current) {
      observer.observe(containerRef.current);
      observerRefValue = containerRef.current;
    }

    return () => {
      if (observerRefValue) {
        observer.unobserve(observerRefValue);
      }
    };
  }, [options]);

  return [containerRef, isVisible];
}

export default useIntersectionObserve;
