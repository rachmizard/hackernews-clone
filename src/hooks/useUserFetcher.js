import { useState, useCallback, useEffect } from "react";
import { getUser } from "services/hnApi";

function useUserFetcher(idx) {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  const fetchData = useCallback(() => {
    setLoading(true);
    getUser(idx).then((data) => {
      setData(data);
      setLoading(false);
    });
  }, [idx]);

  useEffect(() => {
    let subscribe = true;

    if (subscribe) {
      fetchData();
    }

    return () => (subscribe = false);
  }, [fetchData]);

  return [data, loading];
}

export default useUserFetcher;
