import React from "react";
import { categories } from "constant";
import { Switch, Route, Redirect } from "react-router-dom";

const HomePage = React.lazy(() => import("pages/HomePage"));
const StoryDetailPage = React.lazy(() => import("pages/StoryDetailPage"));
const UserPage = React.lazy(() => import("pages/UserPage"));
const UserSubmissionPage = React.lazy(() => import("pages/UserSubmissionPage"));
const UserCommentPage = React.lazy(() => import("pages/UserCommentPage"));
const Page404 = React.lazy(() => import("pages/404"));

const ROUTES = [
  {
    path: "/",
    exact: true,
    render: () => {
      return <Redirect to="/top" />;
    },
  },
  {
    path: "/:type",
    exact: true,
    render: (props) => {
      const { type } = props.match.params;

      if (!categories.includes(type)) {
        return <Redirect to="/top" />;
      }

      return <HomePage {...props} />;
    },
  },
  {
    path: "/item/:itemId",
    exact: true,
    component: StoryDetailPage,
  },
  {
    path: "/user/:userId",
    exact: true,
    component: UserPage,
  },
  {
    path: "/user/:userId/submissions",
    exact: true,
    component: UserSubmissionPage,
  },
  {
    path: "/user/:userId/comments",
    exact: true,
    component: UserCommentPage,
  },
  {
    path: "*",
    component: Page404,
  },
];

export default function RootRouter() {
  return (
    <Switch>
      {ROUTES.map((route) => {
        return <Route key={route.path} {...route} />;
      })}
    </Switch>
  );
}
