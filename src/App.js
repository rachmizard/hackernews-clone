import React, { Suspense } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { GlobalStyle } from "./styles/GlobalStyle.style";
import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from "pages/ErrorFallback";
import { ThemeProvider } from "styled-components";
import { theme } from "constant";
import RootRouter from "routes";
import { Layout } from "components/UI";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <ErrorBoundary FallbackComponent={ErrorFallback}>
        <Router>
          <Suspense fallback={<h5>Loading page...</h5>}>
            <Layout>
              <RootRouter />
            </Layout>
          </Suspense>
        </Router>
      </ErrorBoundary>
    </ThemeProvider>
  );
}

export default App;
